import * as fs from "fs";
import denodeify from 'denodeify';
import * as fontkit from 'fontkit';
import {Collection} from "fontkit";

const readFile = denodeify<string, Buffer>(fs.readFile);

export async function ttc2ttf(fileName: string, postscriptName: string): Promise<Buffer> {
    const buffer = await readFile(fileName);

    if (!isTTCBuffer(buffer)) {
        throw new Error('Invalid TTC file');
    }

    const collection = fontkit.create(buffer) as Collection;
    const font = collection.fonts.find(font => font.postscriptName === postscriptName);

    if (!font) {
        throw new Error(`Requested postscript name is not exist`);
    }

    const ttfOffset = font._directoryPos;

    const tableCount = buffer.readUInt16BE(ttfOffset + 0x04);
    const headerLength = 0x0C + tableCount * 0x10;
    let tableLength = 0;

    for (let j = 0; j < tableCount; j++) {
        const length = buffer.readUInt32BE(ttfOffset + 0x0C + 0x0C + j * 0x10);
        tableLength += ceil4(length);
    }

    const totalLength = headerLength + tableLength;
    const outputBuf = Buffer.alloc(totalLength);
    const headerBuf = buffer.slice(ttfOffset, ttfOffset + headerLength);
    headerBuf.copy(outputBuf);

    let currentOffset = headerLength;

    for (let j = 0; j < tableCount; j++) {
        const offset = buffer.readUInt32BE(ttfOffset + 0x0c + 0x08 + j * 0x10);
        const length = buffer.readUInt32BE(ttfOffset + 0x0c + 0x0C + j * 0x10);

        outputBuf.writeUInt32BE(currentOffset, 0x0C + 0x08 + j * 0x10);
        const tableBuf = buffer.slice(offset, offset + length);
        tableBuf.copy(outputBuf, currentOffset);

        currentOffset += ceil4(length);
    }

    return outputBuf;
}

function ceil4(n: number): number {
    return (n + 3) & ~3;
}

export function isTTCBuffer(buffer: Buffer): boolean {
    return buffer.slice(0, 4).toString() === 'ttcf';
}