import * as yargs from 'yargs';
import {Arguments, Argv} from 'yargs';
import * as fontkit from 'fontkit';
import {Collection} from 'fontkit';
import * as path from "path";
import {ttc2ttf} from "./ttc2ttf";
import * as fs from "fs";

yargs
    .usage('Usage: $0 <command> [target]')
    .command({
        command: 'list <ttc_file>',
        aliases: ['ls', 'l'],
        describe: 'print including ttf list in the ttc file',
        builder(yargs: Argv) {
            return yargs
        },
        handler(argv: Arguments) {
            list(argv.ttc_file);
        }
    })
    .alias('l', 'list')
    .command({
        command: 'extract <ttc_file> [ps_only]',
        describe: 'extract ttf file(s) from the ttc file',
        builder(yargs: Argv) {
            return yargs.option('ps_only', {
                alias: 'p',
                describe: 'extract postscript name matched font only'
            })
        },
        handler(argv: Arguments) {
            const ignored = extract(argv.ttc_file, argv.postscript_name);
        }
    })
    .alias('e', 'extract')
    .help()
    .argv;

const command = yargs.argv._[0];
if (!command) {
    yargs.showHelp();
}

function list(fileName: string): void {
    const ttc = readTTC(fileName);

    console.log('PostScriptName of contained fonts:');
    ttc.fonts.forEach((font, i) => {
        console.log(`  ${i+1}.`, font.postscriptName);
    })
}

async function extract(filePath: string, postscriptName?: string): Promise<void> {
    const ttc = readTTC(filePath);

    if (postscriptName) {
        const font = ttc.fonts.find(font => font.postscriptName === postscriptName);

        if (!font) {
            console.error('Cannot find matching font (%s) from %s', postscriptName, filePath);
            return process.exit(-1);
        }

        const buffer = await ttc2ttf(filePath, font.postscriptName);
        console.log('buffer', buffer.byteLength);
        const outputPath = path.join(path.parse(filePath).dir, postscriptName + '.ttf');
        fs.writeFileSync(outputPath, buffer);
    } else {
        ttc.fonts.forEach(font => extract(filePath, font.postscriptName));
    }
}

function readTTC(fileName: string): Collection {
    let ttc: Collection;

    try {
        ttc = fontkit.openSync(fileName) as Collection;
    } catch (e) {
        console.error('An error (%s) is occured while open ttc file %s', e.message, path.parse(fileName).base);
        process.exit(-1);
    }

    if (!('fonts' in ttc!)) {
        console.error('%s file is not a ttc file.', path.parse(fileName).base);
        process.exit(-1);
    }

    return ttc!;
}