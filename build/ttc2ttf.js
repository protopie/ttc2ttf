"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
}
Object.defineProperty(exports, "__esModule", { value: true });
const util = __importStar(require("util"));
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
function ceil4(n) {
    return (n + 3) & ~3;
}
const readFile = util.promisify(fs.readFile);
async function ttc2ttf(fileName, postscriptName) {
    console.log('fileName', fileName);
    console.log('postscriptName', postscriptName);
    const buffer = await readFile(fileName);
    console.log('isTTCBuffer', isTTCBuffer(buffer));
    const count = getTTFCount(buffer);
    console.log('getTTFCount', count);
    const offsets = getTTFOffsets(buffer, count);
    console.log('getTTFOffsets', offsets);
    for (const tableHeaderOffset of offsets.slice(0, 1)) {
        console.log('-----------------------------------------');
        console.log('table header begins at', tableHeaderOffset);
        const tableCount = buffer.readUInt16BE(tableHeaderOffset + 0x04);
        console.log('tableCount', tableCount);
        const headerLength = 0x0C + tableCount * 0x10;
        console.log('headerLength', headerLength);
        let tableLength = 0;
        for (let j = 0; j < tableCount; j++) {
            const length = buffer.readUInt32BE(tableHeaderOffset + 0x0C + 0x0C + j * 0x10);
            tableLength += ceil4(length);
        }
        console.log('tableLength', tableLength);
        const totalLength = headerLength + tableLength;
        console.log('totalLength', totalLength);
        const outputBuf = Buffer.alloc(totalLength);
        const headerBuf = buffer.slice(tableHeaderOffset, tableHeaderOffset + headerLength);
        console.log('headerBuf', headerBuf.byteLength);
        headerBuf.copy(outputBuf, 0);
        // bufs.push(headerBuf);
        let currentOffset = headerLength;
        for (let j = 0; j < tableCount; j++) {
            const offset = buffer.readUInt32BE(tableHeaderOffset + 0x0c + 0x08 + j * 0x10);
            const length = buffer.readUInt32BE(tableHeaderOffset + 0x0c + 0x0C + j * 0x10);
            console.log('tableOffset, tableLength', offset, length);
            outputBuf.writeUInt32BE(currentOffset, 0x0C + 0x08 + j * 0x10);
            const tableBuf = buffer.slice(offset, offset + length);
            tableBuf.copy(outputBuf, currentOffset);
            // const contentsBuf = buffer.slice(offset, length);
            // bufs.push(contentsBuf);
            currentOffset += ceil4(length);
        }
        fs.writeFileSync(path.join(__dirname, '../Output-' + tableHeaderOffset + '.ttf'), outputBuf);
    }
}
exports.ttc2ttf = ttc2ttf;
function isTTCBuffer(buffer) {
    return buffer.slice(0, 4).toString() === 'ttcf';
}
function getTTFCount(buffer) {
    return buffer.readUInt32BE(0x08);
}
function getTTFOffsets(buffer, count) {
    const ret = [];
    for (let i = 0, offset = 0x0C; i < count; i++) {
        const ttfOffset = buffer.readUInt32BE(offset);
        ret.push(ttfOffset);
        offset += 4;
    }
    return ret;
}
