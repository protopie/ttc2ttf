/// <reference types="node" />
export declare function ttc2ttf(fileName: string, postscriptName: string): Promise<Buffer>;
export declare function isTTCBuffer(buffer: Buffer): boolean;
