"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
}
Object.defineProperty(exports, "__esModule", { value: true });
const yargs = __importStar(require("yargs"));
const fontkit = __importStar(require("fontkit"));
const path = __importStar(require("path"));
const ttc2ttf_1 = require("./ttc2ttf");
const fs = __importStar(require("fs"));
yargs
    .usage('Usage: $0 <command> [target]')
    .command({
    command: 'list <ttc_file>',
    aliases: ['ls', 'l'],
    describe: 'print including ttf list in the ttc file',
    builder(yargs) {
        return yargs;
    },
    handler(argv) {
        list(argv.ttc_file);
    }
})
    .alias('l', 'list')
    .command({
    command: 'extract <ttc_file> [ps_only]',
    describe: 'extract ttf file(s) from the ttc file',
    builder(yargs) {
        return yargs.option('ps_only', {
            alias: 'p',
            describe: 'extract postscript name matched font only'
        });
    },
    handler(argv) {
        const ignored = extract(argv.ttc_file, argv.postscript_name);
    }
})
    .alias('e', 'extract')
    .help()
    .argv;
const command = yargs.argv._[0];
if (!command) {
    yargs.showHelp();
}
function list(fileName) {
    const ttc = readTTC(fileName);
    console.log('PostScriptName of contained fonts:');
    ttc.fonts.forEach((font, i) => {
        console.log(`  ${i + 1}.`, font.postscriptName);
    });
}
async function extract(filePath, postscriptName) {
    const ttc = readTTC(filePath);
    if (postscriptName) {
        const font = ttc.fonts.find(font => font.postscriptName === postscriptName);
        if (!font) {
            console.error('Cannot find matching font (%s) from %s', postscriptName, filePath);
            return process.exit(-1);
        }
        const buffer = await ttc2ttf_1.ttc2ttf(filePath, font.postscriptName);
        console.log('buffer', buffer.byteLength);
        const outputPath = path.join(path.parse(filePath).dir, postscriptName + '.ttf');
        fs.writeFileSync(outputPath, buffer);
    }
    else {
        ttc.fonts.forEach(font => extract(filePath, font.postscriptName));
    }
}
function readTTC(fileName) {
    let ttc;
    try {
        ttc = fontkit.openSync(fileName);
    }
    catch (e) {
        console.error('An error (%s) is occured while open ttc file %s', e.message, path.parse(fileName).base);
        process.exit(-1);
    }
    if (!('fonts' in ttc)) {
        console.error('%s file is not a ttc file.', path.parse(fileName).base);
        process.exit(-1);
    }
    return ttc;
}
