"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
}
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
const denodeify_1 = __importDefault(require("denodeify"));
const fontkit = __importStar(require("fontkit"));
const readFile = denodeify_1.default(fs.readFile);
async function ttc2ttf(fileName, postscriptName) {
    const buffer = await readFile(fileName);
    if (!isTTCBuffer(buffer)) {
        throw new Error('Invalid TTC file');
    }
    const collection = fontkit.create(buffer);
    const font = collection.fonts.find(font => font.postscriptName === postscriptName);
    if (!font) {
        throw new Error(`Requested postscript name is not exist`);
    }
    const ttfOffset = font._directoryPos;
    const tableCount = buffer.readUInt16BE(ttfOffset + 0x04);
    const headerLength = 0x0C + tableCount * 0x10;
    let tableLength = 0;
    for (let j = 0; j < tableCount; j++) {
        const length = buffer.readUInt32BE(ttfOffset + 0x0C + 0x0C + j * 0x10);
        tableLength += ceil4(length);
    }
    const totalLength = headerLength + tableLength;
    const outputBuf = Buffer.alloc(totalLength);
    const headerBuf = buffer.slice(ttfOffset, ttfOffset + headerLength);
    headerBuf.copy(outputBuf);
    let currentOffset = headerLength;
    for (let j = 0; j < tableCount; j++) {
        const offset = buffer.readUInt32BE(ttfOffset + 0x0c + 0x08 + j * 0x10);
        const length = buffer.readUInt32BE(ttfOffset + 0x0c + 0x0C + j * 0x10);
        outputBuf.writeUInt32BE(currentOffset, 0x0C + 0x08 + j * 0x10);
        const tableBuf = buffer.slice(offset, offset + length);
        tableBuf.copy(outputBuf, currentOffset);
        currentOffset += ceil4(length);
    }
    return outputBuf;
}
exports.ttc2ttf = ttc2ttf;
function ceil4(n) {
    return (n + 3) & ~3;
}
function isTTCBuffer(buffer) {
    return buffer.slice(0, 4).toString() === 'ttcf';
}
exports.isTTCBuffer = isTTCBuffer;
