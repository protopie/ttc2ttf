declare module 'fontkit' {
    namespace FontkitNS {
        export interface Font {
            postscriptName: string;
            _directoryPos: number;
        }

        export interface Collection {
            fonts: Font[];
        }

        export interface FontkitExport {
            openSync(filename: string): Font | Collection;
            create(buffer: Buffer): Font | Collection;
        }
    }

    const FontkitNS: FontkitNS.FontkitExport;
    export = FontkitNS;
}
